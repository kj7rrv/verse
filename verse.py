#!/usr/bin/env python3

# verse.py - Bible Verse of the Day

# Copyright (C) 2022 Samuel Sloniker

# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.

# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
# details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import json
import os
import datetime
import subprocess

verses = [
    [
        # January
        "Genesis1:1-3",
        "Romans6:20-23",
        "2Timothy3:16-18",
        "2Peter3:10-11",
        "2Peter3:12-14",
        "Colossians1:16-17",
        "Colossians3:23-25",
        "Revelation3:20-21",
        "Philippians4:6-7",
        "Philippians4:8-9",
        "John3:13-15",
        "John3:16-17",
        "Hebrews12:1-2",
        "1Corinthians13:3-4",
        "2Corinthians12:9-10",
        "1John1:8-10",
        "Matthew28:18-19",
        "Joshua24:14-16",
        "Romans12:19-21",
        "Luke23:42-43",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
    ],
    [
        # February
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
    ],
    [
        # March
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
    ],
    [
        # April
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
    ],
    [
        # May
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
    ],
    [
        # June
        "Hebrews11:1-3",
        "1Chronicles29:11-13",
        "2Timothy2:15-16",
        "1Timothy1:4-7",
        "1Corinthians13:1-3",
        "1Corinthians13:4-7",
        "1Corinthians13:8-10",
        "1Corinthians13:11-13",
        "Psalm37:1-3",
        "Psalm37:4-6",
        "Psalm37:7-10",
        "Revelation3:14-16",
        "Revelation3:17-18",
        "Revelation3:19-22",
        "John14:1-3",
        "John14:4-6",
        "John20:27-29",
        "1Corinthians2:12-13",
        "1Corinthians2:14-16",
        "Psalm97:10-12",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
    ],
    [
        # July
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
    ],
    [
        # August
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
    ],
    [
        # September
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
    ],
    [
        # October
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
    ],
    [
        # November
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
    ],
    [
        # December
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
        "John3:16",
    ],
]


def get_reference():
    date = datetime.datetime.now()
    return verses[date.month - 1][date.day - 1]


def get_verse(length=0):
    return (
        str(
            subprocess.run(
                ["bible", f"-l{length}", get_reference()], capture_output=True
            ).stdout,
            "utf-8",
        )
        .replace("Psalms", "Psalm")
        .replace("Obadiah 1", "Obadiah")
        .replace("Philemon 1", "Philemon")
        .replace("2 John 1", "2 John")
        .replace("3 John 1", "3 John")
        .replace("Jude 1", "Jude")
    )


def to_html(
    text,
    container_tag="div",
    chapter_tag="h2",
    container_class="votd",
    chapter_class="chapter",
    verse_class="verse",
    indent_string="    ",
    indent_level=0,
):
    lines = (i for i in text.split("\n") if i.strip())

    html_lines = [
        f'{indent_string*indent_level}<{container_tag} class="{container_class}">\n',
    ]

    for line in lines:
        if line.startswith("  "):
            if not html_lines[-1].strip()[:3] in ["<li", "<ol"]:
                html_lines.append(f"{indent_string*(indent_level+1)}<ol>\n")

            number, text = line.strip().split(" ", 1)
            html_lines.append(
                f'{indent_string*(indent_level+2)}<li class="{verse_class}" value="{number}">{text}</li>\n'
            )
        else:
            if html_lines[-1].strip()[:3] in ["<li", "<ol"]:
                html_lines.append(f"{indent_string*(indent_level+1)}</ol>\n")

            html_lines.append(
                f'{indent_string*(indent_level+1)}<{chapter_tag} class="{chapter_class}">{line.strip()}</{chapter_tag}>\n'
            )

    if html_lines[-1].strip()[:3] in ["<li", "<ol"]:
        html_lines.append(f"{indent_string*(indent_level+1)}</ol>\n")

    html_lines.append(f"{indent_string*indent_level}</{container_tag}>\n")

    html = "".join(html_lines)

    return html


def html_verse(**kwargs):
    return to_html(get_verse(), **kwargs)

if __name__ == '__main__':
    print(get_verse(min(os.get_terminal_size().columns, 120)))
